<?php

/**
 *  @file
 *  This module handle multiple Tabtimes newsletters.
 *
 *  @copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */
?>

<table class="new-model" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
  <tr>
    <td>
      <p class="view-online">To view this newsletter online, <a href="<?php echo $newsletter->url; ?>">click here</a>.</p>

      <table cellpadding="0" cellspacing="0" border="0" id="inner">
        <tr>
          <td>
            <p class="logo"><?php echo substr($newsletter->title, 0, 7); ?><span class="newsletter-type"><?php echo substr($newsletter->title, 7); ?></span></p>
            <p class="date"><em><?php echo date('F d, Y', $newsletter->dateEnd); ?></em></p>

            <?php if ($newsletter->articles): ?>

            <div id="other-block">
              <?php $counter = 0; ?>
              <?php if(array_key_exists('Feature', $newsletter->articles)): ?>
              <?php foreach ($newsletter->articles['Feature'] as $nid=>$feature):
				    	$node = node_load($nid);
                		$path = drupal_get_path_alias('node/' . $node->nid);
               			$promo = $node->field_promo_text['und'][0]['value'];
                		$article_type = tbhelper_article_type( $node );
                		?>
                  <div class="other-item <?php echo $counter == 1 ? 'first' : 'other'; ?>">
                    <h1><?php echo $article_type['display']; ?></h1>

                    <h2><a href="<?php echo '/' . $path; ?>"><?php echo _tabby7_standard_article_title($node); ?></a></h2>

                    <p class="promo"><?php echo $promo; ?></p>

                    <p class="read-it"><a href="/<?php echo $path; ?>"><strong>Read it &raquo;</strong></a></p>
                  </div>
              <?php endforeach; ?>
              <?php endif; ?>

              <?php unset ($newsletter->articles['Feature']); ?>

              <?php /* Other non-news */ ?>
                <?php $counter = 0; ?>
              <?php foreach ($newsletter->articles as $section => $sub_articles): ?>
				<?php if ($section == 'News' || $section == 'Advertorial'): ?>
				<?php continue; ?>
				<?php endif; ?>
                <?php foreach ($sub_articles as $nid => $sub_article): ?>
                    <?php $counter++; ?>
                    <?php $node = node_load($nid); ?>

                    <?php $path = drupal_get_path_alias('node/' . $node->nid); ?>
                    <?php $promo = $node->field_promo_text['und'][0]['value']; ?>
                    <?php $article_type = tbhelper_article_type( $node ); ?>
                    <div class="other-item <?php echo $counter == 1 ? 'first' : 'other'; ?>">
                      <h1><?php echo $article_type['display']; ?></h1>

                      <h2><a href="<?php echo '/' . $path; ?>"><?php echo _tabby7_standard_article_title($node); ?></a></h2>

                      <p class="promo"><?php echo $promo; ?></p>

                      <p class="read-it"><a href="/<?php echo $path; ?>"><strong>Read it &raquo;</strong></a></p>
                    </div>
                <?php endforeach; ?>
              <?php endforeach; ?>
              <!-- TODO INSERT HERE STATIC AD TEXT -->
              <!-- END STATIC AD TEXT -->
            </div>

            <!-- TODO: INSERT HERE ADVERTORIAL -->
            <!-- END ADVERTORIAL -->

            <div id="news-block">

                <?php $counter = 0; ?>
                <?php if(array_key_exists('News', $newsletter->articles)): ?>
                <h1>News &amp; Trends</h1>
                <ul>
                <?php foreach ($newsletter->articles['News'] as $nid => $news): ?>
<?php $counter++; ?>
                <?php $node = node_load($nid); ?>
                <?php $path = drupal_get_path_alias('node/' . $node->nid); ?>
                <?php $promo = $node->field_promo_text['und'][0]['value']; ?>
                <li class="<?php echo $counter == 1 ? 'first' : 'other'; ?>">
                  <a href="<?php echo '/' . $path; ?>"><?php echo _tabby7_standard_article_title($node); ?></a>

                  <p class="promo"><?php echo $promo; ?></p>

                  <p class="read-it"><a href="/<?php echo $path; ?>"><strong>Read it &raquo;</strong></a></p>
                </li>
                <?php endforeach; ?>
                </ul>
                <?php endif; ?>

            </div>

            <?php endif; ?>

            <div id="footer">
              <p>You are receiving this newsletter at
              <span class="mail">*|EMAIL|*</span> as part of your membership
              with TabTimes.</p>

              <p>To ensure delivery, please add info@tabtimes.com to your
              address book or to your list of approved senders.</p>

              <p>If you prefer not to receive this email, please
              <a href="*|UNSUB|*">unsubscribe</a>.</p>

              <p>If this issue was forwarded to you and you would like to
              receive it at your own address, please register at
              <a href="http://tabtimes.com">TabTimes.com</a>.</p>

              <p>© TabTimes - 41 Madison Avenue, 31st Floor, New York, NY 10010</p>
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
