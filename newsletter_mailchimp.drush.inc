<?php

/**
 * @file
 * Tabtimes newsletters module Drush integration
 *
 * 	Copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 	@author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */

/**
 * Implements hook_drush_command().
 *
 * @return
 *   An associative array of commands.
 */
function newsletter_mailchimp_drush_command() {
  $items = array();

  $items['tt-newsletter-send'] = array(
      'callback' => '_tt_newsletter_send',
      'drupal dependencies' => array( 'mailchimp', 'libtabtimes' ),
      'description' => "Send Tabtimes newsletter task",
      'aliases' => array('ttns'),
      'examples' => array(
          'drush tt-newsletter-send dev media' => 'Send Tabtimes newsletter for developers and for media',
      ),
  );
  $items['tt-newsletter-cron'] = array(
      'callback' => '_tt_newsletter_cron',
      'drupal dependencies' => array( 'mailchimp', 'libtabtimes' ),
      'description' => "Run Tabtimes newsletter periodic task",
      'aliases' => array('ttcr'),
      'examples' => array(
          'drush tt-newsletter-cron' => 'Run Tabtimes newsletter cron task',
      ),
  );
  return $items;
}

function _tt_newsletter_send($newsletter) {
  // Fetch config
  $config = mcNewsletter::config();

  // If multiple arguments
  foreach (func_get_args() as $newsletter_id) {
    if (!array_key_exists($newsletter_id, $config)) {
      throw new Exception('The newsletter passed as argument does not exists');
    }

    // Create the newsletter(s)
    $properties = array(
        'dateEnd' => time(),
        'type' => $newsletter_id,
    );
    $tt_newsletter = new mcNewsletter($properties);

    try {
      $tt_newsletter->sendCampaign();
    } catch (Exception $e) {
      watchdog_exception('tt_newsletter', $e);
    }
  }
}

function _tt_newsletter_cron() {
  tt_newsletter_cron();
}
