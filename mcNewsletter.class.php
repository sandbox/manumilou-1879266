<?php

/**
 *  @file
 *  Class to encapsulate newsletter features and Mailchimp integration
 *
 *  @copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */

class mcNewsletter {

  // Constants
  const SEND_TEST_EMAIL = 'newsletters@tabtimes.com';
  const MODE_INST_MIN = 0;
  const MODE_INST_FULL = 1;
  // End Constants

  // Static Properties
  // End Static Properties

  // Static Methods
  public static function config() {
    return array(
        'dev' => array(
            'id' => 'b681ec8d5b',
            'title' => 'Tablet app development',
            'category' => array(CATEGORY_DEVELOPER),
            'range' => '-2 week',
            'active' => TRUE,
        ),
        'media' => array(
            'id' => '2de9432b8b',
            'title' => 'Tablets for Marketing, Media and Commerce',
            'category' => array(CATEGORY_MEDIA, CATEGORY_MARKETING, CATEGORY_TCOMMERCE),
            'range' => '-1 week',
            'active' => TRUE,
        ),
        'healthcare' => array(
            'id' => '4855689fba',
            'title' => 'Tablets in Healthcare',
            'category' => array(CATEGORY_HEALTHCARE),
            'range' => '-4 week',
            'active' => TRUE,
        ),
        'education' => array(
            'id' => '057ec99153',
            'title' => 'Tablets in Education',
            'category' => array(CATEGORY_EDUCATION),
            'range' => '-4 week',
            'active' => TRUE,
        ),
        'sales' => array(
            'id' => 'bfac1fe5ae',
            'title' => 'Tablets for Sales teams',
            'category' => array(CATEGORY_SALES),
            'range' => '-4 week',
            'active' => TRUE,
        ),
        'picks' => array(
            'id' => '7cb523aebc',
            'title' => 'TabTimes Weekly Update',
            'category' => array(),
            'conditions' => array('sticky' => 1),
            'range' => '-1 week',
            'active' => TRUE,
        ),
        'daily_update' => array(
            'id' => 'ebd3165d88',
            'title' => 'TabTimes Daily Update',
            'range' => '-1 week',
            'active' => FALSE,
        ),
    );
  }
  // End Static Methods

  // Properties
  private $_properties = array(
      'id' => NULL,
      'title' => NULL,
      'type' => NULL,
      'range' => NULL,
      'active' => NULL,
      'dateStart' => NULL,
      'dateEnd' => NULL,
      'mailchimp' => NULL,
      'url' => NULL,
      'articles' => NULL,
      'campaignTitle' => NULL,
  );

  protected $_sectorCategories = NULL;
  protected $_conditions = NULL;
  private $_mode = mcNewsletter::MODE_INST_FULL;
  // End Properties

  // Methods
  public function __construct(array $properties = array(), $mode = mcNewsletter::MODE_INST_FULL) {
    try {
      $this->_mode = $mode;
      $this->_init($properties);
    } catch (Exception $e) {
      echo 'mcNewsletter exception: ',  $e->getMessage(), "\n";
    }
  }


  public function _init(array $properties = array()) {
    global $base_url;
    // $base_url = 'http://tabtimes.com';
    // Load config
    $config = mcNewsletter::config();

    $this->setProperties($properties);

    if (!array_key_exists($this->type, $config)) {
      throw new Exception('Specified newsletter does not exist');
    }

    if (!isset($config[$this->type]['id'])) {
      throw new Exception('Missing Mailchimp ID');
    }

    $this->_sectorCategories = isset($config[$this->type]['category']) ? $config[$this->type]['category'] : FALSE;
    $this->_conditions = isset($config[$this->type]['conditions']) ? $config[$this->type]['conditions'] : FALSE;
    $this->title = $config[$this->type]['title'];
    $this->campaignTitle = $this->title;
    $this->id = $config[$this->type]['id'];
    $this->range = $config[$this->type]['range'];
    $this->active = $config[$this->type]['active'];

    // Compute date range
    $this->dateStart = strtotime( date( 'Y-m-d H:i:s', $this->dateEnd) . $this->range );

    // Set URL
    $this->url = $base_url . '/tt-newsletter/' . $this->type . '/weekly/' . $this->dateEnd;

    // Fetch articles if requested
    if ($this->_mode == mcNewsletter::MODE_INST_FULL) {
      $this->articles = $this->fetchArticles();
    }
  }

  /**
   * Set the properties according to the parameters.
   * Also set newsletter title and sector categories according to configuration
   *
   * @param array $properties
   */
  public function setProperties(array $properties) {
    foreach ($properties as $key => $value) {
      if (array_key_exists($key, $this->_properties) ) {
        $this->{$key} = $value;
      }
    }
  }

  /**
   * Fetch articles
   */
  public function fetchArticles() {

    $categoriesWhere = "";
    $addWhere = "";

    if ($this->_sectorCategories) {
      $categories = implode(", ", $this->_sectorCategories);
      $categoriesWhere = "AND   ( c1.field_primary_category_tid IN ($categories) OR c2.field_secondary_category_tid IN ($categories))";
    }

    if ($this->_conditions) {
      foreach ($this->_conditions AS $col => $val) {
        $addWhere .= " AND {$col} = {$val}";
      }
    }

    $dateStart = date('Y-m-d H:i:s', $this->dateStart);

    $sql = "SELECT DISTINCT(n.nid) FROM {node} n
        LEFT JOIN {field_data_field_date_published} d ON n.nid = d.entity_id AND d.bundle = 'article'
        LEFT JOIN {field_data_field_primary_category} c1 ON n.nid = c1.entity_id AND c1.bundle = 'article'
        LEFT JOIN {field_data_field_secondary_category} c2 ON n.nid = c2.entity_id AND c2.bundle = 'article'
        WHERE n.type = 'article'
        AND   n.status = 1
        AND   d.field_date_published_value >= '$dateStart'
        $categoriesWhere
        $addWhere
        ORDER BY d.field_date_published_value DESC";

    $result = db_query($sql);

    $entities = $result->fetchAll(PDO::FETCH_COLUMN);
    $all_articles = entity_load('node', array_values($entities));

    // Check that current newsletter is not empty
    if (count($all_articles) == 0) {
      return FALSE;
    }

    $articles_by_cat = array();

    // Move array pointer to first element
    reset($all_articles);
    // Set the campaign title here, before splitting by categories
    $this->campaignTitle = $this->title . ' - ' . tbhelper_article_title(current($all_articles));

    foreach ($all_articles as $nid => $article) {
      $type = tbhelper_article_type($article);
      $articles_by_cat[$type['display']][$nid] = $article;
    }

    return $articles_by_cat;
  }

  /**
   * Create the campaign in Mailchimp and send it.
   * Use Mailchimp API
   */
  public function sendCampaign() {

    // Get API access to MailChimp
    $apikey = variable_get('mailchimp_api_key', 0);
    $this->mailchimp = new MCAPI($apikey);

    if (!$this->mailchimp) {
      throw new Exception( 'Mailchimp API has not been properly initialized');
    }

    // Get sending list ID
    $lists = $this->mailchimp->lists();

    $campaign = array();
    foreach ($lists['data'] as $list) {
      if ($list['id'] == $this->id) {
        $campaign = $list;
        break;
      }
    }

    if (empty($campaign)) {
      throw new Exception("Campaign does not exists: " . $this->title);
    }

    // Prepare campaign options
    $options = array(
        'list_id' => $campaign['id'],
        'subject' => $this->campaignTitle,
        'from_email' => 'info@tabtimes.com',
        'from_name' => 'TabTimes'
    );

    // Create the Mailchimp campaign
    $camp_id = $this->mailchimp->campaignCreate('regular', $options, array( 'url' => $this->url));

    // Check for errors
    if ($this->mailchimp->errorCode) {
      throw new Exception( $this->mailchimp->errorMessage );
    }

    // Actually schedule the campaign for sending
    $this->mailchimp->campaignSendNow($camp_id);

    // Check for errors
    if ($this->mailchimp->errorCode) {
      throw new Exception( $this->mailchimp->errorMessage );
    }

    // Save the sent timestamp
    variable_set('tt_newsletter_' . $this->type, $this->dateEnd);

    // Log success
    watchdog('tt_newsletter', 'Newsletter ' . $this->title . ' has been successfully sent');
  }

  public function ready() {

    if (!$this->active) {
      return FALSE;
    }

    $last_sent = variable_get('tt_newsletter_' . $this->type, FALSE);

    // The newsletter has never been sent
    if ($last_sent === FALSE) {
      return TRUE;
    }

    // Compared last sent's date with the range and the current timestamp
    if (intval($last_sent) >= $this->dateStart) {
      // watchdog('tt_newsletter', 'Newsletter ' . $this->title . ' does not need to be sent.');
      return FALSE;
    }

    return TRUE;
  }
  // End Methods


  // Magic Methods
  // End Magic Methods
}
